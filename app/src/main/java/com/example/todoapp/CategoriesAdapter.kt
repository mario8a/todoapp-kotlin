package com.example.todoapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

// Este adapter va pintar las listas y crearlas
class CategoriesAdapter(private val categories: List<TaskCategory>, private  val onItemSelected: (Int) -> Unit) :
    RecyclerView.Adapter<CategoriesViewHolder>() {

    //Crea una visa visual y se lo pasa al metodo onBind pueda pasarle la informacion que va pintar
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_task_category, parent, false)
        return CategoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        holder.render(categories[position], onItemSelected)
    }

    // retorna el tamaño del la lista
    override fun getItemCount(): Int = categories.size

}